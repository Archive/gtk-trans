/***** Includes *****/
#include "trans.h"

/***** Defines *****/
#define RM_ALWAYS 0
#define USE_WIDGETS 1
#define CHECK_LOC 1
#define USE_WIDGET_QUEUE 0

/***** Globals *****/
static int list_timer = -1;
static GList *list = 0;

/*
** anim_init
*/

void anim_init(void)
{
  /* Noise */
  GTRACE((UTTIL_TRACE_FUNC, "+anim_init\n"));

  /* Create the list */
  list = g_list_alloc();

  /* Create the timer */
  list_timer = gtk_timeout_add(1000, anim_cycle, 0);

  /* Noise */
  GTRACE((UTTIL_TRACE_FUNC, "-anim_init\n"));
}

/*
** anim_stop
*/

void anim_stop(void)
{
  /* Noise */
  GTRACE((UTTIL_TRACE_FUNC, "+anim_stop\n"));

  /* Remove the timer */
  if(list_timer > 0) {
    gtk_timeout_remove(list_timer);
  }

  /* Remove the list */
  if(list) {
    g_list_free(list);
  }

  /* All done */
  GTRACE((UTTIL_TRACE_FUNC, "-anim_stop\n"));
}

/*
** anim_add
*/

void anim_add(gint timeout, GdkWindow *window, GtkWidget *widget,
	      gint x, gint y, gint w, gint h)
{
  /* Variables */
  AnimationStruct *data;

  /* Noise */
  GTRACE((UTTIL_TRACE_FUNC,
	      "+anim_add: widget=%p, x=%d, y=%d, w=%d, h=%d\n",
	      widget, x, y, w, h));

  /* Check for the list */
  if(!list) {
    /* Not there */
    GTRACE((UTTIL_TRACE_FUNC, "-anim_add: no list to add.\n"));
    return;
  }

  /* Build the structure */
  data = g_malloc(sizeof(AnimationStruct));

  /* Fill in the data */
  data->widget = widget;
  data->window = window;
  data->x = x;
  data->y = y;
  data->w = w;
  data->h = h;

  /* Check to see if its already there */
  anim_remove(data);

  /* Append it */
  g_list_append(list, data);

  /* All done */
  GTRACE((UTTIL_TRACE_FUNC, "-anim_add: success\n"));
}

/*
** anim_callback
*/

void anim_callback(gpointer sys, gpointer user)
{
  /* Variables */
  XEvent event;
  int x, y, w, h, b, d;
  AnimationStruct *data;
  GtkWidget *widget;
  GdkWindow *win;

  /* Ignore nulls */
  if(!sys) {
    /* System restart */
    return;
  }

#if RM_ALWAYS
  /* Always remove, the redraw will put it back */
  list = g_list_remove(list, sys);
#endif

  /* Make noise */
  GTRACE((UTTIL_TRACE_FUNC, "+anim_callback: widget=%p\n",
	      widget));

  /* Cast it to a window */
  data = (AnimationStruct *) sys;

  /* Pull out the rest of it */
  widget = data->widget;
  win = data->window;

  /* Get some calculated data */
  gdk_window_get_deskrelative_origin(win, &x, &y);
  gdk_window_get_size(win, &w, &h);

  /* Don't bother if nothing changed */
#if CHECK_LOC
  if(x == data->x &&
     y == data->y &&
     w == data->w &&
     h == data->h) {
#if !RM_ALWAYS
    /* Remove it */
    list = g_list_remove(list, sys);
#endif
    GTRACE((UTTIL_TRACE_FUNC, "-anim_callback: nothing changed.\n"));
    return;
  }
#endif

  /* Save it in the structure */
  data->x = x;
  data->y = y;
  data->w = w;
  data->h = h;

  /* Check to see if we can do this */
  if(!GTK_IS_WIDGET(widget) ||
     !GTK_WIDGET_TOPLEVEL(widget) ||
     !GTK_WIDGET_DRAWABLE(widget)) {
    /* Can't draw to it */
#if !RM_ALWAYS
    /* Remove it */
    list = g_list_remove(list, sys);
#endif

    /* Nope, remove it from the list */
    GTRACE((UTTIL_TRACE_FUNC, "-anim_callback: invalid, removed\n"));
    return;
  }

  /* gtk-single_connect(widget, "delete_event",
     GTK_SINGAL_FUNC(mycallback)); */

#if !USE_WIDGETS
  /* gdk_window_clear_area_e */
  gdk_window_clear_area_e(win,
			  x, y, w, h);
  /* Clear the window */
  //  gdk_window_clear(win);

  /* Flush it */
  gdk_flush();
#else
  /* Draw it */
  printf("Queue widget drawing.\n");
#if USE_WIDGET_QUEUE
  gtk_widget_queue_draw(widget);
#else
  gtk_widget_draw(widget, NULL);
#endif
#endif

  /* End message */
  GTRACE((UTTIL_TRACE_FUNC, "-anim_callback: success\n"));
}

/*
** anim_cycle
*/

gint anim_cycle(gpointer data)
{
  /* Go through each list */
  g_list_foreach(list, anim_callback, 0);
  
  /* Keep doing it */
  return TRUE;
}

/*
** anim_is_in
*/

gboolean
anim_remove(AnimationStruct *data)
{
  /* Variables */
  GList *l = list, *n;
  GtkWidget *widget = data->widget;
  AnimationStruct *d;

  /* Noise */
  GTRACE((UTTIL_TRACE_FUNC, "+anim_is_in: data=%p\n", data));

  /* Go through the list */
  while(l) {
    /* Pull it out */
    d = l->data;

    /* Make sure there is something there */
    if(d) {
      /* Check for match */
      if(d->widget == widget) {
	n = l->next;
	g_list_remove_link(list, l);
	l = n;
	continue;
      }
    }

    /* Next one */
    l = l->next;
  }

  /* Nope */
  GTRACE((UTTIL_TRACE_FUNC, "-anim_is_in: no\n"));
  return FALSE;
}

