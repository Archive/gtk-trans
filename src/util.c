/***** Includes *****/
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "trans.h"

/*
** gtrace
*/

void gtrace(long lvl, char *fmt, ...)
{
  /* Variables */
  va_list args;
  long env_lvl = 0;
  char *env_str;

  /* Get the environment variable to see if we want logging. */
  env_str = getenv("GTK_TRANS_LOG");
  if(env_str && *env_str) {
    /* Set it */
    env_lvl = atol(env_str);
  }

  /* Check for a level match (simple check to see if bits match) */
  if(env_lvl | lvl) {
    /* Get the variable arguments */
    va_start(args, fmt);
    
    /* Just print it */
    fprintf(stderr, "UTTIL: ");
    vfprintf(stderr, fmt, args);
    
    /* Finish it */
    va_end(args);
  }
}
